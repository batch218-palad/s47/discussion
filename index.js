// The "document" refers to the whole webpage
// The 'querySelector' is used to select a spcfc object (HTML elements) from the webpage (document)
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// alternative, we can use getElement functions to retrieve the elements
// const spanFullName = document.getElementById('span-full-name');

// The "addEventListener" function that takes to argument
//keyup - string indentfying the event
//event => {} - function that the listener will execute once the specified event is triggered
txtFirstName.addEventListener('keyup', (event)=> {
	// The 'innerHTML' property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event)=> {
	// The 'event.target' contains the element where the event happened
	console.log(event.target);
	// The 'event.taget.value' gets the value of the input object
	console.log(event.target.value);
});






